# OCR Sample

This will show how text extraction with golang

## Requirements:

### Tesseract

#### install on linux
- sudo apt install tesseract-ocr
- sudo apt install libtesseract-dev

#### install on mac 
- sudo port install tesseract

### gosseract package
* go get -t github.com/otiai10/gosseract

## Run:
* Go to folder ocr_poc and run command "go run main.go"

