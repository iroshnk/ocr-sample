package main

import (
	"bufio"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/otiai10/gosseract"
)

func main() {
	//fileToBeUploaded := "image.png"
	fileToBeUploaded := "images/image.png"

	// Open file on disk.
	imageFile, err := os.Open(fileToBeUploaded)
	if err != nil {
		fmt.Println("error: ", err)
		return
	}

	// Read entire JPEG into byte slice.
	reader := bufio.NewReader(imageFile)
	content, err := ioutil.ReadAll(reader)
	if err != nil {
		fmt.Println("error: ", err)
		return
	}

	// Encode as base64.
	encodedString := base64.StdEncoding.EncodeToString(content)

	//base64 string to bytes array.
	data, err := base64.StdEncoding.DecodeString(encodedString)
	if err != nil {
		fmt.Println("error:", err)
		return
	}

	client := gosseract.NewClient()
	defer client.Close()
	client.SetImageFromBytes(data)
	text, _ := client.Text()

	fmt.Printf("Text : %s", text)
}
