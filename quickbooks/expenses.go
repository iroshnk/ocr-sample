package quickbooks

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
)

// Purchase represents a QuickBooks Purchase object.
type Purchase struct {
	ID        string
	SyncToken string
	domain    string
	MetaData  MetaData

	PurchaseEx PurchaseEx

	TxnDate     Date
	TotalAmt    json.Number
	PaymentType string
	sparse      bool
	AccountRef  ReferenceType

	//LinkedTxn
	Line []Line

	CustomField []string
}

// MetaData is a timestamp of genesis and last change of a Quickbooks object
type MetaData struct {
	CreateTime      Date
	LastUpdatedTime Date
}

// ReferenceType represents a QuickBooks reference to another object.
type ReferenceType struct {
	Value string
	Name  string
}

// ValueReferenceType represents a QuickBooks reference to another object.
type ValueReferenceType struct {
	Value string
}

// PurchaseEx ...
type PurchaseEx struct {
	any []PurchaseExAny
}

// PurchaseExAny ...
type PurchaseExAny struct {
	name string
	//nil bool
	value           ReferenceType
	declaredType    string
	scope           string
	globalScope     bool
	typeSubstituted bool
}

// Line ...
type Line struct {
	ID                            string
	Amount                        json.Number
	PaymentType                   string
	AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail
}

// AccountBasedExpenseLineDetail ...
type AccountBasedExpenseLineDetail struct {
	TaxCodeRef     ValueReferenceType
	BillableStatus string
	AccountRef     ReferenceType
}

// PurchaseCreateApi ...
type PurchaseCreateApi struct {
	PaymentType string
	AccountRef  ReferenceType

	Line []PurchaseCreateLineApi
}

//PurchaseCreateLineApi ...
type PurchaseCreateLineApi struct {
	Amount                        json.Number
	PaymentType                   string
	AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail
}

// CreatePurchase creates the given Purchase on the QuickBooks server, returning
// the resulting Purchase object.
func CreatePurchase(inv *PurchaseCreateApi, c *http.Client) (*Purchase, error) {
	var u, err = url.Parse("https://sandbox-quickbooks.api.intuit.com")
	if err != nil {
		return nil, err
	}
	u.Path = "/v3/company/" + RealmID + "/purchase"
	var v = url.Values{}
	v.Add("minorversion", minorVersion)
	u.RawQuery = v.Encode()
	var j []byte
	j, err = json.Marshal(inv)
	if err != nil {
		return nil, err
	}
	var req *http.Request
	req, err = http.NewRequest("POST", u.String(), bytes.NewBuffer(j))
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	var res *http.Response
	res, err = c.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return nil, errors.New("Error")
	}

	var r struct {
		Purchase Purchase
		time     Date
	}
	err = json.NewDecoder(res.Body).Decode(&r)
	return &r.Purchase, err
}
